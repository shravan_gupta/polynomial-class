﻿/*
* PolynomialClass.cpp
*
* Class Description: Implementation on Polynomial Class.
* Class Invariant: Index of the array is the exponent of the corresponding term. 
*	    If a term is missing, then it simply has a zero coefficient.
*                   
* Author: Shravan Gupta
*/

#include "stdafx.h"
#include<iostream>
using namespace std;

class Polynomial
{
private:
	double* arr;
	int size;
public:
	Polynomial();
	Polynomial(double* poly1, int size);
	Polynomial(const Polynomial& original);
	~Polynomial();

	friend ostream& operator<<(ostream& out, Polynomial poly);
	friend Polynomial operator+(const double value, const Polynomial& poly1);
	friend Polynomial operator-(const double value, const Polynomial& poly1);
	friend Polynomial operator*(const double value, const Polynomial& poly1);

	Polynomial operator+(const Polynomial& poly1);
	Polynomial operator+(const double value);
	Polynomial operator-(const Polynomial& poly1);
	Polynomial operator-(const int value);
	Polynomial operator*(const Polynomial& poly1);
	Polynomial operator*(const double value);
	Polynomial operator=(const Polynomial& original);
	double &operator[](int index);

	double evaluate(double value);
	int getSize()const;
};

Polynomial::Polynomial()
{
	size = 1;
	arr = new double[size];
	arr[0] = NULL;
}

Polynomial::Polynomial(double* poly1, int length)
{
	size = length;
	arr = new double[size];
	for (int i = 0; i < size; i++)
	{
		arr[i] = poly1[i];
	}
}

Polynomial::Polynomial(const Polynomial& original)
{
	size = original.size;
	arr = new double[size];
	for (int i = 0; i < size; i++)
	{
		arr[i] = original.arr[i];
	}
}

//Destructor
Polynomial ::~Polynomial()
{
	delete[] arr;
}

int Polynomial::getSize()const
{
	return size;
}

double &Polynomial::operator[](int index)
{
	return arr[index];
}

Polynomial Polynomial::operator=(const Polynomial& original)
{
	size = original.size;
	arr = new double[size];
	for (int i = 0; i < size; i++)
	{
		arr[i] = original.arr[i];
	}
	return Polynomial(arr, size);
}

double Polynomial::evaluate(double value)
{
	double sum = 1;
	double count = arr[0];
	for (int i = 1; i < size; i++)
	{
		sum = 1;
		for (int j = 0; j < i; j++)
		{
			sum = sum*value;
		}
		sum = sum*arr[i];
		count = count + sum;
	}
	return count;
}

Polynomial operator+(const double value, const Polynomial& poly1)
{
	Polynomial poly2;
	poly2.size = poly1.size;
	poly2.arr = new double[poly2.size];
	poly2.arr[0] = value + poly1.arr[0];
	for (int i = 1; i < poly2.size; i++)
	{
		poly2.arr[i] = poly1.arr[i];
	}
	return poly2;
}

Polynomial operator- (const double value, const Polynomial& poly1)
{
	Polynomial poly2;
	poly2.size = poly1.size;
	poly2.arr = new double[poly2.size];
	poly2.arr[0] = value - poly1.arr[0];
	for (int i = 1; i < poly2.size; i++)
	{
		poly2.arr[i] = -poly1.arr[i];
	}
	return poly2;
}

Polynomial operator*(const double value, const Polynomial& poly1)
{
	Polynomial poly2;
	poly2.size = poly1.size;
	poly2.arr = new double[poly2.size];
	for (int i = 0; i < poly2.size; i++)
	{
		poly2.arr[i] = poly1.arr[i] * value;
	}
	return poly2;
}

Polynomial Polynomial ::operator*(const double value)
{
	Polynomial poly1;
	poly1.size = size;
	poly1.arr = new double[poly1.size];
	for (int i = 0; i < poly1.size; i++)
	{
		poly1.arr[i] = arr[i] * value;
	}
	return poly1;
}

Polynomial Polynomial :: operator+(const double value)
{
	Polynomial poly1;
	poly1.size = size;
	poly1.arr = new double[poly1.size];
	poly1.arr[0] = arr[0] + value;
	for (int i = 1; i < poly1.size; i++)
	{
		poly1.arr[i] = arr[i];
	}
	return poly1;
}

Polynomial Polynomial::operator-(const int value)
{
	Polynomial poly1;
	poly1.size = size;
	poly1.arr = new double[poly1.size];
	poly1.arr[0] = arr[0] - value;
	for (int i = 1; i < poly1.size; i++)
	{
		poly1.arr[i] = arr[i];
	}
	return poly1;
}

Polynomial Polynomial:: operator+(const Polynomial& poly1)
{
	Polynomial poly2;
	if (size >= poly1.size)
	{
		poly2.size = size;
		poly2.arr = new double[poly2.size];
		for (int i = 0; i < poly1.size; i++)
		{
			poly2.arr[i] = arr[i] + poly1.arr[i];
		}
		for (int i = poly1.size; i < size; i++)
		{
			poly2.arr[i] = arr[i];
		}
		return poly2;
	}
	else
	{
		poly2.size = poly1.size;
		poly2.arr = new double[poly2.size];
		for (int i = 0; i < size; i++)
		{
			poly2.arr[i] = arr[i] + poly1.arr[i];
		}
		for (int i = size; i < poly2.size; i++)
		{
			poly2.arr[i] = poly1.arr[i];
		}
		return poly2;
	}
}

Polynomial Polynomial :: operator*(const Polynomial& poly1)
{
	Polynomial poly2;
	poly2.size = size + poly1.size - 1;
	poly2.arr = new double[poly2.size];
	for (int i = 0; i < poly2.size; i++)
	{
		poly2.arr[i] = 0;
	}
	if (size <= poly1.size)
	{
		int count;
		for (int i = 0; i < size; i++)
		{
			count = i;
			for (int j = 0; j < poly1.size; j++)
			{
				poly2.arr[count] = poly2.arr[count] + (arr[i] * poly1.arr[j]);
				count++;
			}
		}
		return poly2;
	}
	else
	{
		int count;
		for (int i = 0; i < poly1.size; i++)
		{
			count = i;
			for (int j = 0; j < size; j++)
			{
				poly2.arr[count] = poly2.arr[count] + (poly1.arr[i] * arr[j]);
			}
		}
		return poly2;
	}
}

Polynomial Polynomial :: operator-(const Polynomial& poly1)
{
	Polynomial poly2;
	if (size > poly1.size)
	{
		poly2.size = size;
		poly2.arr = new double[poly2.size];
		for (int i = 0; i < poly1.size; i++)
		{
			poly2.arr[i] = arr[i] - poly1.arr[i];
		}
		for (int i = poly1.size; i < size; i++)
		{
			poly2.arr[i] = arr[i];
		}
		return poly2;
	}
	else
	{
		poly2.size = poly1.size;
		poly2.arr = new double[poly2.size];
		for (int i = 0; i < size; i++)
		{
			poly2.arr[i] = arr[i] - poly1.arr[i];
		}
		for (int i = size; i < poly2.size; i++)
		{
			poly2.arr[i] = -poly1.arr[i];
		}
		return poly2;
	}
}

ostream& operator<<(ostream& out, Polynomial poly)
{
	int count = 1;
	out << poly.arr[0];
	for (int i = 1; i < poly.size; i++)
	{
		if (poly.arr[i] >= 0)
		{
			if (poly.arr[i] == 0)
			{
				count++;
				continue;
			}
			out << " + " << poly.arr[i] << "x^" << count;
			count++;
		}
		else
		{
			out << " " << poly.arr[i] << "x^" << count;
			count++;
		}
	}
	return out;
}
int main()
{

	cout << "Testing Polynomial class:\n";
	Polynomial empty;
	cout << "Empty polynomial is " << empty << endl << endl;

	double one[] = { 1,2 };
	Polynomial One(one, 1);
	double quad[] = { 3, 2, 1 };
	double five[] = { 1, 2, 4, 3.2, 5 };
	Polynomial q(quad, 3);
	Polynomial c(five, 5);

	cout << "Polynomial One is:  " << One << endl << endl;
	cout << "Polynomial q is:  " << q << endl << endl;
	cout << "Polynomial c is:  " << c << endl << endl;

	q[1] = 8.2;
	cout << "After q[1] = 8.2, q is:  " << q << endl << endl;

	Polynomial p = q;
	cout << "Ater  Polynomial p = q , polynomial p is:  " << p << endl << endl;
	q[1] = 0.5;
	cout << "Now, polynomial p is still:  " << p << endl << endl;

	Polynomial r;
	r = q = c;
	c[0] = 100;
	cout << "After r = q = c, polynomial r is:  " << r << endl;
	cout << "After r = q = c, polynomial q is:  " << q << endl;

	cout << "Polynomial c:" << endl;
	for (int i = 0; i < 5; i++)
		cout << "   term with degree " << i << " has coefficient " << c[i] << endl;

	cout << endl << "value of p(3.5) is " << p.evaluate(3.5) << endl;
	cout << "value of r(2) is " << r.evaluate(2) << endl;

	r = q + r;
	cout << endl << "After r = q + r, polynomial r is:  " << r << endl;
	cout << "value of r(2) is " << r.evaluate(2) << endl << endl;

	r = One - q;
	cout << "value of (One - q)(2) is " << r.evaluate(2) << endl << endl;

	r = q * c;
	cout << "size of q*c is " << r.getSize() << endl;
	cout << "Polynomial q is:  " << q << endl;
	cout << "Polynomial c is:  " << c << endl;
	cout << "Polynomial r (= q*c) is:  " << r << endl << endl;

	cout << "value of r(2) is " << r.evaluate(2) << endl << endl;

	cout << "5 + q makes polynomial  " << 5 + q << endl << endl;
	cout << "5 * q makes polynomial  " << 5 * q << endl << endl;
	cout << "q - 5 makes polynomial  " << q - 5 << endl << endl;

	system("pause");
	return 0;
}